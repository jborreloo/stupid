from slackclient import SlackClient

BOT_ID = 'U49DZS5LZ'

AT_BOT = "<@" + BOT_ID + ">"

sc = SlackClient('xoxb-145475889713-xVvKxs5SBdkzZ4whuBBJOGBQ')

def handle_command(command, channel):
    response = "I AM A COMPUTER"

    sc.api_call("chat.postMessage", channel=channel,
            text=response, as_user=True)

def parse_slack_output(slack_rtm_output):
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output and 'text' in output and AT_BOT in output['text']:
                return output['text'].split(AT_BOT)[1].strip().lower(), \
                        output['channel']
                return None, None

if __name__ == "__main__":
    READ_WEBSOCKET_DELAY = 1
    if sc.rtm_connect():
        print("StarterBot connected and running!")
        while True:
            command, channel = parse_slack_output(sc.rtm_read())
            if command and channel:
                handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
        else:
            print("Connection failed")
